public class Cercle{   
   private double rayon;
   ...   
   public boolean equals(Object obj) {
      //s ils ont la meme adresse memoire, ils sont//egaux.
      if (this == obj) return true;
      //si l objet en parametre est null, ils ne sont pas//egaux.
      if (obj == null) return false;
      //Si les objets ne sont pas de la meme classe, ils //ne sont pas egaux.
      if (getClass() != obj.getClass()) return false; 
      //On transforme le parametre en un objet de type //Cercle et on compare les rayons.
      Cercle cercle = (Cercle) obj;
      return this.rayon == cercle.rayon;
   }
   public int hashCode(){
      return ((Double)rayon).hashCode();
   }
}