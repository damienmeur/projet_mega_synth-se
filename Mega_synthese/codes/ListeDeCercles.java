import java.util.ArrayList;
public class ListeDeCercles{
   private ArrayList<Cercle> cercles; 
   public ListeDeCercles(){      
      cercles = new ArrayList<Cercle>();   
   }
   public void ajouter(Cercle cercle){ 
      if (!this.contient(cercle)) 
      cercles.add(cercle);   
   }   
   public void supprimer (Cercle cercle){
      cercles.remove(cercle);
   }
   public boolean contient(Cercle cercle){ 
      return cercles.contains(cercle);
   }
}