public class Cercle {
   private double rayon;
   public Cercle (double rayon){
      this.rayon = rayon;    
   }
   public void setRayon (double rayon){//setRayon et double sont la signature de la methode
      this.rayon = rayon;    
   }    
   public double getRayon(){//getRayon et l'absence de parametre sont la signature de la methode
      return rayon;
   }
}