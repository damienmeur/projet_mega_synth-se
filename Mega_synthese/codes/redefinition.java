public class Cercle{  
   private double rayon;    
   public String toString(){
      return "Cercle de rayon " + rayon; 
   }
}

public class CerclePlace extends Cercle{
   private Point centre;
   ...
   public String toString() {
      return super.toString() + " centre en " + centre;  
   }
   ... 
}