public class TestPolymorphisme{  
   public static void main(String[] args){
      Point point = new Point(2,3);
      Cercle cercle = new CerclePlacé(1,point);
      System.out.println(cercle.toString()); 
      System.out.println("aire du cercle : " +cercle.calculerAire());
   }