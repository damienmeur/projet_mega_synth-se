/*
*
*@autor Damien Meur
*
*/
public class F5D1{
   public static java.util.Scanner scanner = new java.util.Scanner(System.in);
   public static void main(String[] args){
      
      //initialiser table
      String[] table=new String[16];
      for(int i=5;i<16;i++){
         table[i]="vide";
      }
      for(int i=0;i<=5;i++){
         if(i%2==0) table[i]="blanc";
         else table[i]="noir";
      }
         
      //initialiser jeu
      int nombreJetonBlanc=3;
      int nombreJetonNoir=3;
      String joueurCourant;
      System.out.println("Quel est le nom du joueur 1 ?(il aura les jetons blancs)");
      String joueur1=scanner.nextLine();
      System.out.println("Quel est le nom du joueur 2 ?(il aura les jetons noirs)");
      String joueur2=scanner.nextLine();
      joueurCourant=joueur1;
      
      //partie en cours
      while(nombreJetonBlanc>0 && nombreJetonNoir>0 && table[15]=="vide"){
         //debut du tour
         String couleurEnCours="";
         if(joueurCourant==joueur1) couleurEnCours="blanc";
         if(joueurCourant==joueur2) couleurEnCours="noir";
         System.out.println("C'est au tour de "+joueurCourant+"\n\n");
         afficherTable(table);
         int lancerDeDes=unEntierAuHasardEntre(0,6);
         System.out.println("\nValeur du lancer de des : "+lancerDeDes);
         System.out.println("Le jeton de quelle case souhaitez-vous deplacer ?");
         int jetonADeplacer=scanner.nextInt();
         
         //deplacement jeton
         if(jetonADeplacer<=15 && jetonADeplacer>=0 && table[jetonADeplacer]==couleurEnCours){
            int nouvelleCase=jetonADeplacer+lancerDeDes;
            table[jetonADeplacer]="vide";
            
            if(nouvelleCase<=15 && table[nouvelleCase]!="vide"){
               if(table[nouvelleCase]=="blanc") nombreJetonBlanc--;
               else if(table[nouvelleCase]=="noir") nombreJetonNoir--;
               table[nouvelleCase]=couleurEnCours;
            }
            if(nouvelleCase<=15&& table[nouvelleCase]=="vide"){
               table[nouvelleCase]=couleurEnCours;
            }
            if(nouvelleCase>15){
               table[15]=couleurEnCours;
            }
          }
         
          //changement joueur
         if(joueurCourant==joueur1) joueurCourant=joueur2;
         else if(joueurCourant==joueur2) joueurCourant=joueur1;
      }
      
      afficherTable(table);
      //Qui a gagné ?
      String gagnant;
      if(nombreJetonNoir==0 || table[15]=="blanc"){
         gagnant=joueur1;
      }
      else gagnant=joueur2;
      System.out.println("Le gagnant est : "+ gagnant);
   }

   public static void afficherTable(String[] table){
      for(int j=0;j<105;j++){
         System.out.print("-");
      }
      System.out.print("\n");
      System.out.print("||");
      for(int i=0;i<16;i++){
         System.out.print(table[i]+"||");
      }
      System.out.print("\n");
      for(int j=0;j<105;j++){
         System.out.print("-");
      }
      System.out.print("\n");
   }
      
   public static int unEntierAuHasardEntre (int valeurMinimale, int valeurMaximale){
      double nombreReel;
      int resultat;
      nombreReel = Math.random();
      resultat = (int) (nombreReel * (valeurMaximale - valeurMinimale + 1))
         					+ valeurMinimale;
      return resultat;
   }
}


      
